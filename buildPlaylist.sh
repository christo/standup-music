#!/bin/bash

. /etc/profile
. /Users/christo/.bash_profile

if [ -d /Volumes/devfiles/standup-music ]; then
    rsync -vaur /Volumes/devfiles/standup-music ~/
fi

find ~/standup-music -name '*.mp3' ! -name '*Spice*' > ~/.playlist
find ~/standup-music -name '*.og*' ! -name '*Spice*'  >> ~/.playlist
find ~/standup-music -name '*.wm*' ! -name '*Spice*'  >> ~/.playlist
